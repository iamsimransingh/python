from django.db import models

# Create your models here.
class feedback(models.Model):
    name= models.CharField(max_length=200)
    email= models.EmailField(max_length=250)
    message = models.TextField()
    Date = models.DateTimeField(auto_now_add=True, blank = True)


    def __str__(self):
        return self.message

    class Meta:
        verbose_name_plural = 'Feedback'

class scoreboard(models.Model):
    Right_Answers= models.CharField(max_length=200)
    Wrong_Answers= models.CharField(max_length=250)
    Date = models.DateTimeField(auto_now_add=True, blank = True)

    def __str__(self):
        return repr(self.Date)

    class Meta:
        verbose_name_plural = 'Score Board'
