from django.conf.urls import url
from myapp import views
app_name ='quiz'
urlpatterns = [
    url('question/',views.quiz, name='question'),
    #url('result/',views.result, name='result'),
    url('quiz/',views.facts, name='facts'),

]
