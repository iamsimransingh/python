from django.shortcuts import render
from myapp.models import feedback, scoreboard
from django.http import HttpResponse
import requests
# Create your views here.
def index(request):
    if request.method =='POST':
        name = request.POST['name']
        email=request.POST['email']
        message=request.POST['message']


        feedbackobj = feedback(name=name,email=email,message=message)

        feedbackobj.save()
        print('data saved')

    return render(request, 'index.html')

def quiz(request):
    api = requests.get('http://sachtechsolution.com/dailydose/admin/webservice/questionsProcess.php?type=questions')
    mydata= api.json()
    right = 0
    wrong = 0
    result = {'myans':'active'}
    count = 1
    mynewdata = []
    for q in mydata['Questions']:

        dic = {'id':count,'question':q['question'],'qid':q['ques_id'],'a':q['option1'],'b':q['option2'],'c':q['option3'],'d':q['option4']}
        mynewdata.append(dic)
        count += 1
    #print(mynewdata)
    for a in mydata['Questions']:
        if request.method == 'POST' and request.POST:
            if a['answer'] == request.POST[a['ques_id']]:
                right += 1
            else:
                wrong += 1


    result['right'] = right;
    result['wrong'] = wrong;
    print(result)
    if result['right'] == 0 or result['wrong'] == 0:
        return render(request, 'quiz.html',context = {'quiz':mynewdata})
    else:
        scores = scoreboard(Right_Answers=right, Wrong_Answers=wrong)
        scores.save()
        print('saved successfully')
        board = scoreboard.objects.order_by('Date')


        return render(request, 'result.html',context = {'quiz':result,'des':mydata,'board':board})

def facts(request):
    apfact = requests.get('http://sachtechsolution.com/dailydose/admin/webservice/factProcess.php?type=getLanguageFacts&languageId=3')
    data =apfact.json()
    return render(request,'fact.html',context = {'fact':data['facts']})
